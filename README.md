# Get Started

#Setup Authentication for Package Registry

1. Project Settings > Repository > Deploy Tokens.
2. Enter the required details and provide the `read_package_registry` and `write_package_registry` permissions to install/publish packages to the registry.
3. You’ll see a username and a `token`.

Use `token` for publish package

#Initial project nodejs

```sh
npm init # Using npm
```

#Update the following information in your package.json

1. Package name
2. Set root file
3. Add publish config

```
{
  ...
  "name": "@scope/<your-package-name>"
  ...
  "main": "./src/index.js",
  "type": "module",
  "files": [
    "src"
  ]
  ...
  "publishConfig": {
    "@scope:registry": "https://<your-domain>/api/v4/projects/<your-project-id>/packages/npm/"
  }
  ...
}
```

#Create a .npmrc at the root of your project.

```sh
touch .npmrc
```

#Update the following information in your .npmrc

```.npmrc
# Set URL for your scoped packages.
# For example package with name `@scope/my-package` will use this URL for download
@scope:registry=https://gitlab.com/api/v4/packages/npm/

# Add the token for the scoped packages URL. This will allow you to download
# `@scope/` packages from private projects.
'//gitlab.com/api/v4/packages/npm/:_authToken'="${GITLAB_AUTH_TOKEN}"

# Add token for uploading to the registry. Replace <your-project-id>
# with the project you want your package to be uploaded to.
'//gitlab.com/api/v4/projects/<your-project-id>/packages/npm/:_authToken'="${GITLAB_AUTH_TOKEN}"

```

- `scope` is root level group of project

#Publish your package

```sh
GITLAB_AUTH_TOKEN=$AUTH_TOKEN npm publish

```

#Deploy .gitlab-ci.yml to configure Gitlab CI job
```sh
cp .gitlab-ci.example.yml .gitlab-ci.yml
```